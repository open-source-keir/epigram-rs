use lapin::{Channel, BasicProperties, Connection};
use lapin::options::BasicPublishOptions;
use crate::error::EpigramError::{MessagePublishError, BuilderIncomplete};
use crate::error::EpigramError;
use crate::header::Header;

// Wait for async traits...
pub trait Producer {
    fn publish_message(&self, key: Option<Vec<u8>>, value: Vec<u8>, headers: Option<Vec<Header>>) -> Result<(), EpigramError>;
}

pub struct RabbitMQProducer {
    channel: Channel,
}

impl RabbitMQProducer {
    pub async fn new(connection: &Connection) -> Self {
        let channel = connection
            .create_channel()
            .await
            .expect("Failed to create RabbitMQ Producer channel with provided TCP connection");

        RabbitMQProducer::builder()
            .channel(channel)
            .build()
            .expect("Failed to build epigram RabbitMQProducer")
    }

    pub fn builder() -> RabbitMQProducerBuilder {
        RabbitMQProducerBuilder::new()
    }

    pub async fn publish_message(&self, topic: String, value: Vec<u8>) -> Result<(), EpigramError> {
        let publisher_confirm = match self.channel.basic_publish(
            "",
            &*topic,
            BasicPublishOptions::default(),
            value,
            BasicProperties::default()
        ).await {
            Ok(publisher_confirm) => publisher_confirm,
            Err(err) => return Err(MessagePublishError(err.to_string()))
        };

        match publisher_confirm.await {
            Ok(_) => Ok(()),
            Err(err) => Err(MessagePublishError(err.to_string()))
        }
    }
}

pub struct RabbitMQProducerBuilder {
    channel: Option<Channel>,
}

impl RabbitMQProducerBuilder {
    pub fn new() -> Self {
        Self {
            channel: None
        }
    }

    pub fn channel(mut self, value: Channel) -> Self {
        self.channel = Some(value);
        self
    }

    pub fn build(self) -> Result<RabbitMQProducer, EpigramError> {
        if let Some(channel) = self.channel {
            Ok(RabbitMQProducer {channel})
        } else {
            Err(BuilderIncomplete())
        }
    }
}