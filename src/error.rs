use thiserror::Error;

#[derive(Error, Debug)]
pub enum EpigramError {
    #[error("Failed to build due to incomplete attributes provided")]
    BuilderIncomplete(),

    #[error("Failed to publish message")]
    MessagePublishError(String),
}