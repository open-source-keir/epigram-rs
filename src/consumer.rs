use crate::error::EpigramError;
use serde::Deserialize;
use lapin::{Connection, Consumer as LapinConsumer, types::FieldTable};
use crate::error::EpigramError::BuilderIncomplete;
use lapin::options::BasicConsumeOptions;
use futures_lite::stream::NextFuture;
use futures_lite::StreamExt;

// Wait for async traits...
pub trait Consumer {
    fn consume_message(&self) -> Result<(), EpigramError>;
}

pub struct RabbitMQConsumer {
    consumer: LapinConsumer,
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub topic: String,
    pub consumer_id: String,
}

impl RabbitMQConsumer {
    pub async fn new(cfg: &Config, connection: Connection) -> Self {
        let channel = connection
            .create_channel()
            .await
            .expect("Failed to create RabbitMQ Consumer channel with provided TCP connection");

        let consumer = channel.basic_consume(
            &*cfg.topic,
            &*cfg.consumer_id,
            BasicConsumeOptions::default(),
            FieldTable::default(),
        ).await.expect("Failed to construct internal lapin consumer instance");

        RabbitMQConsumer::builder()
            .consumer(consumer)
            .build()
            .expect("Failed to build epigram RabbitMQProducer")
    }

    pub fn builder() -> RabbitMQConsumerBuilder {
        RabbitMQConsumerBuilder::new()
    }

    pub fn next(&mut self) -> NextFuture<'_, LapinConsumer> {
        self.consumer.next()
    }
}

pub struct RabbitMQConsumerBuilder {
    consumer: Option<LapinConsumer>,
}

impl RabbitMQConsumerBuilder {
    pub fn new() -> Self {
        Self {
            consumer: None,
        }
    }

    pub fn consumer(mut self, value: LapinConsumer) -> Self {
        self.consumer = Some(value);
        self
    }

    pub fn build(self) -> Result<RabbitMQConsumer, EpigramError> {
        if let Some(consumer) = self.consumer {
            Ok(RabbitMQConsumer {consumer})
        } else {
            Err(BuilderIncomplete())
        }
    }
}